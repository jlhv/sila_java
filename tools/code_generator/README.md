# Code Generator
Code generator parsing XML to Java Class and creating protobuf definitions.

Future functionality is open for discussion.

## Building and Running
To install the package, simply use maven:

    mvn install
    
Then the JARs can be deployed on machines running JDK 11:

    cd target/
    java -jar code_generator.jar [path_to_xml] [path_to_proto]
    
To use for example the TestFeature:

    java -jar target/code_generator.jar src/test/resources/TestFeature.xml TestFeature.proto
    
## Command Line Tool
Note that his only works for Unix systems.

To call the code generator handily, use the install script (only works from directory):

```bash
cd unix_install
./install.sh
```

After installation, the code generator can be called from terminal via terminal:
```bash
sila_code_gen [path_to_feature_def_xml/feature_def.xml] [path_to_generated_proto/generated_proto.proto]
```