package sila_java.library.server_base.binary_transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Blob;

@Getter
@AllArgsConstructor
public class Binary {
    private final Blob data;
    private final BinaryInfo info;
}
