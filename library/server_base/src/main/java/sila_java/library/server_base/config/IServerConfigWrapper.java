package sila_java.library.server_base.config;

import lombok.NonNull;

import java.io.IOException;

public interface IServerConfigWrapper {

    /**
     * @return the current configuration in memory previously loaded / set / generated
     */
    ServerConfiguration getCacheConfig();

    /**
     * Set and save a new configuration
     * @param serverConfiguration A non null configuration to set
     * @throws IOException If unable to save the new configuration
     */
    void setConfig(@NonNull ServerConfiguration serverConfiguration) throws IOException;
}
