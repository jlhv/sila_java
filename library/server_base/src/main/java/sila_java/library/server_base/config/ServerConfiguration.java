package sila_java.library.server_base.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import javax.annotation.concurrent.Immutable;
import java.util.UUID;

@Immutable
@Getter
@AllArgsConstructor
public final class ServerConfiguration {
    private final String name;
    private final UUID uuid;

    /**
     * Generate a default server config using the server type and a random generate UUID
     * @param serverType The server type
     * @return A default server config
     */
    public static ServerConfiguration generateDefault(@NonNull final String serverType) {
        return new ServerConfiguration(serverType, UUID.randomUUID());
    }

    /**
     * @param name The name of the server
     * @return a new instance with the specified name
     */
    public ServerConfiguration withName(@NonNull final String name) {
        return new ServerConfiguration(name, this.uuid);
    }
}
