package sila_java.library.server_base.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import sila_java.library.core.communication.utils.ListNetworkInterfaces;
import sila_java.library.core.utils.GitRepositoryState;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Slf4j
public class ArgumentHelper {
    @Getter private final String[] args;
    @Getter private final Namespace ns;

    /**
     * Parse the main arguments
     * If the list network flag is specified, a list of internet interface will be displayed and the program will exit
     * @param args The main arguments
     * @param serverName The server name
     */
    public ArgumentHelper(@NonNull final String[] args,
                          @NonNull final String serverName) {
        this(args, serverName, null);
    }

    /**
     * Parse the main arguments
     * If the list network flag is specified, a list of internet interface will be displayed and the program will exit
     * @param args The main arguments
     * @param serverName The server name
     * @param baseParser If you want to extend the default parser with other flags
     */
    public ArgumentHelper(@NonNull final String[] args,
                          @NonNull final String serverName,
                          final ArgumentParser baseParser) {
        this.args = args;

        final ArgumentParser parser;
        // Argument Parser
        if (baseParser == null) {
            parser = ArgumentParsers.newFor(serverName).build()
                    .defaultHelp(true)
                    .description("SiLA Server.");
        } else {
            parser = baseParser;
        }
        // Port Argument (optional)
        parser.addArgument("-p", "--port")
                .type(Integer.class)
                .help("Specify port to use.");
        // Network Interface Argument
        parser.addArgument("-n", "--networkInterface")
                .type(String.class)
                .help(
                    "Specify network interface. Check using the ifconfig command on Linux or MacOS and ipconfig on Windows."
                );
        // Server config file holding UUID and ServerName
        parser.addArgument("-c", "--configFile")
                .type(String.class)
                .help("Specify the file name to use to read/store server information.");
        // Only list System Information
        parser.addArgument("-l", "--listNetworks")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("List names of network interfaces found.");
        // Encrypt communication or not
        parser.addArgument("-e", "--encryption")
            .type(Arguments.booleanType("yes", "no"))
            .setDefault(true)
            .help("Specify if the server must encrypt communication or not.");
        parser.addArgument("-s", "--simulation")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("Specify (if supported) to start the server with simulation mode enabled.");
        parser.addArgument("-v", "--version")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("Display server version.");
        this.ns = parser.parseArgsOrFail(args);
        if (ns.getBoolean("listNetworks")) {
            ListNetworkInterfaces.display();
            System.exit(0);
        }
        if (ns.getBoolean("version")) {
            try {
                System.out.println(new GitRepositoryState().generateVersion());
                System.exit(0);
            } catch (IOException e) {
                log.warn("Unable to retrieve version", e);
                System.out.println("Unknown version");
                System.exit(0);
            }
        }
    }

    public Optional<Integer> getPort() {
        final Integer port = ns.getInt("port");
        return (port == null) ? Optional.empty() : Optional.of(port);
    }

    public Optional<String> getInterface() {
        final String networkInterface = ns.getString("networkInterface");
        return (networkInterface == null) ? Optional.empty() : Optional.of(networkInterface);
    }

    public Optional<Path> getConfigFile() {
        final String configFile = ns.getString("configFile");
        return (configFile == null) ? Optional.empty() : Optional.of(Paths.get(configFile));
    }

    public boolean isSimulationEnabled() {
        return ns.getBoolean("simulation");
    }

    public boolean useEncryption() {
        return ns.getBoolean("encryption");
    }
}
