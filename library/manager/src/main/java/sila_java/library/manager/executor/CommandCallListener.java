package sila_java.library.manager.executor;

import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.CallInProgress;

public interface CommandCallListener {
    default void onProgress(CallInProgress callInProgress, CancellationToken cancellationToken) {}

    default void onComplete(CallCompleted callCompleted) {}

    default void onError(CallErrored callErrored) {}
}