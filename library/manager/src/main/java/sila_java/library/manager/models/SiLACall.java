package sila_java.library.manager.models;

import lombok.*;
import sila_java.library.manager.ServerManager;

import java.util.UUID;

/**
 * Represents a generic SiLA Call
 */
@Getter
public final class SiLACall {
    private final UUID identifier = UUID.randomUUID();
    private final UUID serverId;
    private final String featureId;
    private final String callId;
    private final Type type;
    private final String parameters;

    public enum Type {
        UNOBSERVABLE_COMMAND,
        OBSERVABLE_COMMAND,
        UNOBSERVABLE_PROPERTY,
        OBSERVABLE_PROPERTY
    }

    /**
     * Call Constructor
     * @param serverId As added to the {@link ServerManager}
     * @param featureId As defined in the Feature Definition
     * @param callId Either a Command or Property Identifier
     * @param type Different Call Types: Unobservable command, Observable command, unobservable & Observable Properties
     * @param parameters Parameters in Protobuf JSON Format as seen on the protobuf documentation
     *
     * @see <a href="https://developers.google.com/protocol-buffers/docs/proto3#json">Protobuf Spec</a>
     */
    public SiLACall(
            @NonNull final UUID serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type,
            @NonNull final String parameters
    ) {
        this.serverId = serverId;
        this.featureId = featureId;
        this.callId = callId;
        this.type = type;
        this.parameters = parameters;
    }

    public SiLACall(
            @NonNull final UUID serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type
    ) {
        this(serverId, featureId, callId, type, "{}");
    }
}
