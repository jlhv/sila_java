package sila_java.library.manager.executor;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.CallInProgress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
public class CommandProgressMonitor implements CommandCallListener {
    @Getter
    private final Map<UUID, CallInProgress> callsInProgress = new ConcurrentHashMap<>(); // Collections.synchronized* can raise an exception if iterating while a change is made to the collection
    @Getter
    private final List<CallCompleted> callsCompleted =  new CopyOnWriteArrayList<>();
    @Getter
    private final List<CallErrored> callsErrored =  new CopyOnWriteArrayList<>();
    private final Map<UUID, CancellationToken> cancellationTokens = new ConcurrentHashMap<>();

    public void cancelCommand(@NonNull final UUID identifier) {
        final CancellationToken cancellationToken = cancellationTokens.get(identifier);
        if (cancellationToken != null) {
            cancellationToken.cancel();
            log.info("Cancelled command with identifier {}", identifier);
        } else {
            log.warn("Attempted to cancel a command already cancelled or removed with id {}", identifier);
        }
    }

    @Override
    public void onProgress(CallInProgress callInProgress, CancellationToken cancellationToken) {
        cancellationTokens.put(callInProgress.getSiLACall().getIdentifier(), cancellationToken);
        callsInProgress.put(callInProgress.getSiLACall().getIdentifier(), callInProgress);
    }

    @Override
    public void onComplete(CallCompleted callCompleted) {
        cancellationTokens.remove(callCompleted.getSiLACall().getIdentifier());
        callsInProgress.remove(callCompleted.getSiLACall().getIdentifier());
        callsCompleted.add(callCompleted);
    }

    @Override
    public void onError(CallErrored callErrored) {
        cancellationTokens.remove(callErrored.getSiLACall().getIdentifier());
        callsInProgress.remove(callErrored.getSiLACall().getIdentifier());
        callsErrored.add(callErrored);
    }
}