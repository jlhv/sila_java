package sila_java.library.manager.executor;

public interface CancellationToken {
    void cancel();
}
