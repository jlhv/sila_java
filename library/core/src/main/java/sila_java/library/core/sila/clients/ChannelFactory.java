package sila_java.library.core.sila.clients;

import io.grpc.ManagedChannel;
import io.grpc.netty.NettyChannelBuilder;
import io.netty.handler.ssl.SslContext;
import lombok.NonNull;
import javax.annotation.Nullable;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLException;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.encryption.EncryptionUtils.buildTLSContextNoCert;

/**
 * Custom Channel Builder
 */
@Slf4j
public class ChannelFactory {
    private static final int CHANNEL_IDLE_TIMEOUT = 5; // [s]
    private static SslContext sslContext = null;

    /**
     * Build a new gRPC Channel instance related to a SiLA Server
     *
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return A new channel connected to Server
     */
    public static ManagedChannel withoutEncryption(@NonNull final String host, final int port) {
        return buildChannel(host, port, null);
    }

    /**
     * Build a new gRPC (Netty) Channel instance with TLS related to a SiLA Server
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return A new channel connected to Server
     *
     * @implNote Accepts untrusted certificates from the server
     */
    public static ManagedChannel withEncryption(@NonNull final String host, final int port) {
        try {
            if (sslContext == null) {
                sslContext = buildTLSContextNoCert();
            }
        } catch (SSLException e) {
            log.error("Unable to build TLS Context", e);
            throw new RuntimeException(e);
        }

        return buildChannel(host, port, sslContext);
    }

    private static ManagedChannel buildChannel(
            @NonNull final String host,
            final int port,
            @Nullable final SslContext sslContext
    ) {
        final NettyChannelBuilder channelBuilder =  NettyChannelBuilder
                .forAddress(host, port)
                .idleTimeout(CHANNEL_IDLE_TIMEOUT, TimeUnit.SECONDS);
        if (sslContext != null) {
            channelBuilder
                    .sslContext(sslContext)
                    .useTransportSecurity();
        } else {
            channelBuilder.usePlaintext();
        }

        return channelBuilder.build();
    }
}
