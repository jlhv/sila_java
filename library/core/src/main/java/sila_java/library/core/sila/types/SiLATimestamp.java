package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLATimestamp {
    public static SiLAFramework.Timestamp from(final OffsetDateTime offsetDateTime) {
        return SiLAFramework.Timestamp
                .newBuilder()
                .setTimezone(SiLATimeZone.from(offsetDateTime.getOffset()))
                .setYear(offsetDateTime.getYear())
                .setMonth(offsetDateTime.getMonthValue())
                .setDay(offsetDateTime.getDayOfMonth())
                .setHour(offsetDateTime.getHour())
                .setMinute(offsetDateTime.getMinute())
                .setSecond(offsetDateTime.getSecond())
                .build();
    }
}
