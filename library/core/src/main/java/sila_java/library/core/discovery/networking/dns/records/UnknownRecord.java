package sila_java.library.core.discovery.networking.dns.records;

import lombok.NonNull;

/**
 * Handle records that are not necessary for DNS-SD
 */
class UnknownRecord extends Record {
    UnknownRecord(@NonNull final String name, final long ttl) {
        super(name, ttl);
    }
}
