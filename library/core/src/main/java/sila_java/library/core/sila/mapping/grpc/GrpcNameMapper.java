package sila_java.library.core.sila.mapping.grpc;

/**
 * Collection of SiLA gRPC Name Mapping
 */
public class GrpcNameMapper {
    private final static String parameterSuffix = "_Parameters";
    private final static String responseSuffix = "_Responses";

    private final static String stateCommandSuffix = "_Info";
    private final static String intermediateCommandSuffix = "_Intermediate";
    private final static String intermediateResponseSuffix = "_IntermediateResponses";
    private final static String resultSuffix = "_Result";

    private final static String unobservablePropertyPrefix = "Get_";
    private final static String observablePropertyPrefix = "Subscribe_";

    private final static String metadataPrefix = "Metadata_";
    private final static String metadataRPCPrefix = "Get_FCPAffectedByMetadata_";

    private final static String dataTypePrefix = "DataType_";
    private final static String structSuffix = "_Struct";

    public static String getParameter(final String parameter) {
        return (parameter + GrpcNameMapper.parameterSuffix);
    }

    public static String getResponse(final String response) {
        return (response + GrpcNameMapper.responseSuffix);
    }

    public static String getStateCommand(final String stateCommand) {
        return (stateCommand + GrpcNameMapper.stateCommandSuffix);
    }

    public static String getIntermediateCommand(final String intermediateCommand) {
        return (intermediateCommand + GrpcNameMapper.intermediateCommandSuffix);
    }

    public static String getIntermediateResponse(final String intermediateResponse) {
        return (intermediateResponse + GrpcNameMapper.intermediateResponseSuffix);
    }

    public static String getResult(final String result) {
        return (result + GrpcNameMapper.resultSuffix);
    }

    public static String getUnobservableProperty(final String unobservableProperty) {
        return (GrpcNameMapper.unobservablePropertyPrefix + unobservableProperty);
    }

    public static String getObservableProperty(final String observableProperty) {
        return (GrpcNameMapper.observablePropertyPrefix + observableProperty);
    }

    public static String getMetadata(final String metadata) {
        return (GrpcNameMapper.metadataPrefix + metadata);
    }

    public static String getMetadataRPC(final String metadata) {
        return (GrpcNameMapper.metadataRPCPrefix + metadata);
    }

    public static String getDataType(final String dataType) {
        return (GrpcNameMapper.dataTypePrefix + dataType);
    }

    public static String getStruct(final String struct) {
        return (struct + GrpcNameMapper.structSuffix);
    }
}
