package sila_java.library.core.sila.mapping.feature;

/**
 * This Exception is thrown if certain descriptions in the SiLA Feature Definition
 * is not complying with the standard.
 */
public class MalformedSiLAFeature extends Exception {
    public MalformedSiLAFeature(String message) {
        super(message);
    }

    public MalformedSiLAFeature(Throwable cause) {
        super(cause);
    }
}
