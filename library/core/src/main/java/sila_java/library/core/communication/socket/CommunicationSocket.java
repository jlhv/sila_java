package sila_java.library.core.communication.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Bidirectional Communication being managed by {@see SynchronousCommunication}
 */
public abstract class CommunicationSocket {
    private boolean isClosed = true;

    public void open() throws IOException {
        if (!isClosed) return;
        openSocket();
        isClosed = false;
    }

    public void close() throws IOException {
        closeSocket();
        isClosed = true;
    }

    public abstract OutputStream getOutputStream() throws IOException;

    public abstract InputStream getInputStream() throws IOException;

    /**
     * Open communication socket, will only be called when closed
     */
    abstract void openSocket() throws IOException;

    /**
     * Closing the socket, can be called anytime
     */
    abstract void closeSocket() throws IOException;
}

