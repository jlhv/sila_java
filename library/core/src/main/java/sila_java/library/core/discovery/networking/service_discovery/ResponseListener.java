package sila_java.library.core.discovery.networking.service_discovery;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.discovery.networking.dns.Response;
import sila_java.library.core.discovery.networking.dns.records.PtrRecord;
import sila_java.library.core.discovery.networking.dns.records.Record;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Listens to DNS Responses and Updates instance Cache
 */
@Slf4j
public class ResponseListener implements Consumer<Response> {
    private final InstanceListener instanceListener;
    private final String serviceType;

    public interface InstanceListener {
        void instanceAdded(Instance instance);
    }

    public ResponseListener(@NonNull final InstanceListener instanceListener, @NonNull final String serviceType) {
        this.instanceListener = instanceListener;
        this.serviceType = serviceType;
    }

    @Override
    public void accept(@NonNull final Response response) {
        log.debug("Listener response: {}", response);

        // Add Instance if PTR Record exists and Service Type is matched
        final Optional<Record> ptrRecord = response.getRecords()
                .stream()
                .filter(record -> record instanceof PtrRecord && record.getName().contains(serviceType))
                .findFirst();

        ptrRecord.ifPresent(record -> {
            final PtrRecord ptr = (PtrRecord) record;
            try {
                final Optional<Instance> optionalInstance = Instance.createFromRecords(ptr, response.getRecords());
                if (optionalInstance.isPresent()) {
                    if (optionalInstance.get().getTtl() > 0) {
                        instanceListener.instanceAdded(optionalInstance.get());
                    } else {
                        log.debug("Instance {} TTL is 0", optionalInstance.get().getName());
                    }
                }
            } catch (final Exception e) {
                log.debug(e.getMessage());
            }
        });
    }
}
