package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAString {
    public static SiLAFramework.String from(final String str) {
        return SiLAFramework.String.newBuilder().setValue(str).build();
    }
}
