package sila_java.library.core.discovery.networking.dns.records;

import lombok.Getter;
import lombok.NonNull;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * The record AAAA (also quad-A record) specifies IPv6 address for given host.
 * So it works the same way as the A record and the difference is the type of IP address.
 */
@Getter
public class AaaaRecord extends Record {
    private final InetAddress address;

    public AaaaRecord(
            @NonNull final ByteBuffer buffer,
            @NonNull final String name,
            final long ttl
    ) throws UnknownHostException {
        super(name, ttl);
        final byte[] addressBytes = new byte[16];
        buffer.get(addressBytes);
        address = InetAddress.getByAddress(addressBytes);
    }

    @Override
    public String toString() {
        return "AaaaRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", address=" + address +
                '}';
    }
}
