package sila_java.library.core.discovery.networking.service_discovery;

public class InvalidInstanceNameException extends Exception {
    public InvalidInstanceNameException(String message) {
        super(message);
    }
}
