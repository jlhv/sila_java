package sila_java.library.core.discovery.networking.dns.records;

import lombok.Getter;
import lombok.NonNull;

import java.nio.ByteBuffer;

/**
 * An SRV record typically defines a symbolic name and
 * the transport protocol used as part of the domain name
 */
@Getter
public class SrvRecord extends Record {
    private final int priority;
    private final int weight;
    private final int port;
    private final String target;

    public SrvRecord(@NonNull final ByteBuffer buffer, @NonNull final String name, final long ttl) {
        super(name, ttl);
        priority = buffer.getShort() & USHORT_MASK;
        weight = buffer.getShort() & USHORT_MASK;
        port = buffer.getShort() & USHORT_MASK;
        target = readNameFromBuffer(buffer);
    }

    @Override
    public String toString() {
        return "SrvRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", priority=" + priority +
                ", weight=" + weight +
                ", port=" + port +
                ", target='" + target + '\'' +
                '}';
    }
}
