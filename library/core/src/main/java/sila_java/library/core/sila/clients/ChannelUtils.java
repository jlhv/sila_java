package sila_java.library.core.sila.clients;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila_java.library.core.asynchronous.MethodPoller;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * Utilities for gRPC Channel Management
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ChannelUtils {
    /**
     * Waits until gRPC channel is in a ready state, waits until the connection is up
     * @param managedChannel Channel to check
     * @param timeout Maximum time to wait for, if not met throws exception
     */
    public static void waitUntilReady(
            @NonNull ManagedChannel managedChannel,
            @NonNull Duration timeout) throws TimeoutException, ExecutionException {
        MethodPoller.await()
                .withInterval(Duration.ofSeconds(2))
                .atMost(timeout)
                .until(()-> managedChannel.getState(true).equals(ConnectivityState.READY));
    }
}
