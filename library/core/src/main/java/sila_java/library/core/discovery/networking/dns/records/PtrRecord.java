package sila_java.library.core.discovery.networking.dns.records;

import lombok.Getter;
import lombok.NonNull;

import java.nio.ByteBuffer;

/**
 * As opposed to forward DNS resolution (A and AAAA DNS records),
 * the PTR record is used to look up domain names based on an IP address.
 */
@Getter
public class PtrRecord extends Record {
    private final static String UNTITLED_NAME = "Untitled";
    private final String userVisibleName;
    private final String ptrName;

    public PtrRecord(@NonNull final ByteBuffer buffer, @NonNull final String name, final long ttl, final int rdLength) {
        super(name, ttl);
        if (rdLength > 0) {
            ptrName = readNameFromBuffer(buffer);
        } else {
            ptrName = "";
        }
        userVisibleName = buildUserVisibleName();
    }

    @Override
    public String toString() {
        return "PtrRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", ptrName='" + ptrName + '\'' +
                '}';
    }

    private String buildUserVisibleName() {
        final String[] parts = ptrName.split("\\.");
        if (parts[0].length() > 0) {
            return parts[0];
        } else {
            return UNTITLED_NAME;
        }
    }
}
