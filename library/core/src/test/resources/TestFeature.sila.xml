<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" Originator="ch.unitelabs"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
    <!-- Reference Feature to test SiLA Code Generation, also serves as a reference
     for FDL to IDL generation capabilities -->

    <Identifier>TestFeature</Identifier>
    <DisplayName>Test Feature</DisplayName>
    <Description>Test feature to test SiLA code generation</Description>
    <Command>
        <Identifier>SayHello</Identifier>
        <DisplayName>Say Hello</DisplayName>
        <Description>Does what it says</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>Name</Identifier>
            <DisplayName>Name</DisplayName>
            <Description>Your Name</Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                    <Constraints>
                        <MinimalLength>1</MinimalLength>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>Greeting</Identifier>
            <DisplayName>Greeting</DisplayName>
            <Description>The greeting coming back at you</Description>
            <DataType>
                <Basic>Binary</Basic>
            </DataType>
        </Response>
        <DefinedExecutionErrors>
            <Identifier>TestError</Identifier>
            <Identifier>TestError2</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <Command>
        <Identifier>GetPosition</Identifier>
        <DisplayName>Get Position</DisplayName>
        <Description>Get the current position</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>PositionEstimate</Identifier>
            <DisplayName>Position Estimate</DisplayName>
            <Description>Position estimate to compare.</Description>
            <DataType>
                <DataTypeIdentifier>Position</DataTypeIdentifier>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>Index</Identifier>
            <DisplayName>Index</DisplayName>
            <Description>Index of Position</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Parameter>
            <Identifier>WaitUntilFound</Identifier>
            <DisplayName>Wait Until Found</DisplayName>
            <Description>Indication of waiting procedure</Description>
            <DataType>
                <Basic>Boolean</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentPosition</Identifier>
            <DisplayName>Current Position</DisplayName>
            <Description>Yep, current position</Description>
            <DataType>
                <DataTypeIdentifier>Position</DataTypeIdentifier>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>ControlTemperature</Identifier>
        <DisplayName>Control Temperature</DisplayName>
        <Description>Control the Temperature gradually to a set Target,</Description>
        <Observable>Yes</Observable>
        <Parameter>
            <Identifier>TargetTemperature</Identifier>
            <DisplayName>Target Temperature</DisplayName>
            <Description>
                The target temperature that the device will try to reach. Note that the command might
                be completed at a temperature that is close enough.
            </Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                    <Constraints>
                        <Unit>
                            <Label>K</Label>
                            <Factor>1</Factor>
                            <Offset>0</Offset>
                            <UnitComponent>
                                <SIUnit>Kelvin</SIUnit>
                                <Exponent>0</Exponent>
                            </UnitComponent>
                        </Unit>
                        <MaximalInclusive>373.0</MaximalInclusive>
                        <MinimalExclusive>273.0</MinimalExclusive>
                    </Constraints>
                </Constrained>
            </DataType>
        </Parameter>
        <IntermediateResponse>
            <Identifier>CurrentTemperature</Identifier>
            <DisplayName>Current Temperature</DisplayName>
            <Description>Current Temperature being controled</Description>
            <DataType>
                <Constrained>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                    <Constraints>
                        <Unit>
                            <Label>K</Label>
                            <Factor>1</Factor>
                            <Offset>0</Offset>
                            <UnitComponent>
                                <SIUnit>Kelvin</SIUnit>
                                <Exponent>0</Exponent>
                            </UnitComponent>
                        </Unit>
                    </Constraints>
                </Constrained>
            </DataType>
        </IntermediateResponse>
        <DefinedExecutionErrors>
            <Identifier>TemperatureNotReachable</Identifier>
        </DefinedExecutionErrors>
    </Command>
    <DataTypeDefinition>
        <Identifier>Position</Identifier>
        <DisplayName>Position</DisplayName>
        <Description>Vector in x-y-z</Description>
        <DataType>
            <Structure>
                <Element>
                    <Identifier>X</Identifier>
                    <DisplayName>X</DisplayName>
                    <Description>X position value</Description>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Y</Identifier>
                    <DisplayName>Y</DisplayName>
                    <Description>Y position value</Description>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Z</Identifier>
                    <DisplayName>Z</DisplayName>
                    <Description>Z position value</Description>
                    <DataType>
                        <Basic>Real</Basic>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
    </DataTypeDefinition>
    <Property>
        <Identifier>DummyProperty</Identifier>
        <DisplayName>Dummy Property</DisplayName>
        <Description>A random dummy property</Description>
        <Observable>No</Observable>
        <DataType>
            <Basic>Real</Basic>
        </DataType>
    </Property>
    <Property>
        <Identifier>MyMood</Identifier>
        <DisplayName>My Mood</DisplayName>
        <Description>Dynamic Display of my mood</Description>
        <Observable>Yes</Observable>
        <DataType>
            <Constrained>
                <DataType>
                    <Basic>String</Basic>
                </DataType>
                <Constraints>
                    <Set>
                        <Value>Happy</Value>
                        <Value>Sad</Value>
                    </Set>
                </Constraints>
            </Constrained>
        </DataType>
    </Property>
    <Property>
        <Identifier>ImplementedFeatures</Identifier>
        <DisplayName>Implemented Features</DisplayName>
        <Description>Returns a list of qualified Feature identifiers of all implemented Features of
            this SiLA Device.</Description>
        <Observable>No</Observable>
        <DataType>
            <List>
                <DataType>
                    <Structure>
                        <Element>
                            <Identifier>Something</Identifier>
                            <DisplayName>Something</DisplayName>
                            <Description>Something</Description>
                            <DataType>
                                <Basic>String</Basic>
                            </DataType>
                        </Element>
                    </Structure>
                </DataType>
            </List>
        </DataType>
    </Property>
    <Metadata>
        <Identifier>UserData</Identifier>
        <DisplayName>User Data</DisplayName>
        <Description>Example of some User Data being sent</Description>
        <DataType>
            <Structure>
                <Element>
                    <Identifier>Name</Identifier>
                    <DisplayName>Name</DisplayName>
                    <Description>Name of User</Description>
                    <DataType>
                        <Basic>String</Basic>
                    </DataType>
                </Element>
                <Element>
                    <Identifier>Age</Identifier>
                    <DisplayName>Age</DisplayName>
                    <Description>Age of User</Description>
                    <DataType>
                        <Constrained>
                            <DataType>
                                <Basic>Integer</Basic>
                            </DataType>
                            <Constraints>
                                <Unit>
                                    <Label>Year</Label>
                                    <Factor>31536000</Factor>
                                    <Offset>0</Offset>
                                    <UnitComponent>
                                        <SIUnit>Second</SIUnit>
                                        <Exponent>1</Exponent>
                                    </UnitComponent>
                                </Unit>
                            </Constraints>
                        </Constrained>
                    </DataType>
                </Element>
            </Structure>
        </DataType>
    </Metadata>
    <DefinedExecutionError>
        <Identifier>TestError</Identifier>
        <DisplayName>Test Error</DisplayName>
        <Description>Test Error</Description>
    </DefinedExecutionError>
    <DefinedExecutionError>
        <Identifier>TestError2</Identifier>
        <DisplayName>Test Error 2</DisplayName>
        <Description>Test Error 2</Description>
    </DefinedExecutionError>
</Feature>