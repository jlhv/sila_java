# SiLA Java Release Process
This document is a work in progress. Currently we do very simple manual releases, this will be changed mid 2019.

The release process will include:
- Branching and tagging the release
- Releasing with appropriate versions and snapshots on Maven Central
- Release Notes and notification of Community

### Local setup
#### Create a key
Linux:
- Create a new key (if you don't already have one) `gpg2 --gen-key`
#### Publish a key
Linux:
- List the keys `gpg2 --list-keys`
- Publish the key (replace `<KEY-ID>` by yours) `gpg2 --keyserver hkp://ipv4.pool.sks-keyservers.net --send-keys <KEY-ID>`
#### Configure Maven
- Create / edit the file `~/.m2/settings.xml`and add the following lines into it :
```xml
<settings>
  <servers>
    <server>
      <id>ossrh</id>
      <username><USERNAME></username>
      <password><PASSWORD></password>
    </server>
  </servers>
  <profiles>
    <profile>
      <id>ossrh</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <gpg.executable>gpg2</gpg.executable>
        <gpg.passphrase><PASS_PHRASE></gpg.passphrase>
      </properties>
    </profile>
  </profiles>
</settings>
```
- Replace `<USERNAME>` with your SonaType username
- Replace `<PASSWORD>` with your SonaType password
- Replace `<PASS_PHRASE>` with your key passphrase
### Deploy artifacts
- If deploying a release, upgrade the version of the artifacts in the poms
- `mvn clean deploy`
- Go to `https://oss.sonatype.org/#stagingRepositories` and login into your SonaType account
- Select the deployed repository, then you can either `release` if everything is correct or `drop`.
### Use of released artifacts
Just add the dependencies like you would normally do in your project pom like so:
```xml
<dependency>
    <groupId>org.sila-standard.sila_java.library</groupId>
    <artifactId>manager</artifactId>
    <version>0.0.2</version>
</dependency>
```