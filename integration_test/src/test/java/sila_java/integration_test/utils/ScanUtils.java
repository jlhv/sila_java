package sila_java.integration_test.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila_java.library.core.asynchronous.MethodPoller;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.models.Server;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ScanUtils {
    public static void scanAndWaitServerState(
            @NonNull final UUID serverId,
            @NonNull final Server.Status status,
            final int timeout
    ) {
        ServerFinder
                .filterBy(ServerFinder.Filter.uuid(serverId), ServerFinder.Filter.status(status))
                .scanAndFindOne(Duration.ofSeconds(timeout))
                .orElseThrow(RuntimeException::new);
    }

    public static void waitServerState(
            @NonNull final UUID serverId,
            @NonNull final Server.Status status,
            final int timeout
    ) throws TimeoutException, ExecutionException {
        MethodPoller
                .await()
                .atMost(Duration.ofSeconds(timeout))
                .withInterval(Duration.ofMillis(100))
                .until(() -> ServerFinder
                        .filterBy(ServerFinder.Filter.uuid(serverId), ServerFinder.Filter.status(status))
                        .findOne()
                        .isPresent());
    }
}
