package sila_java.examples.test_server.impl;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestGrpc;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.SiLAString;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ObservableProperty extends ObservablePropertyTestGrpc.ObservablePropertyTestImplBase {
    public static final int NB_RESPONSE = 10;

    @Override
    public void subscribeListString(ObservablePropertyTestOuterClass.Subscribe_ListString_Parameters request, StreamObserver<ObservablePropertyTestOuterClass.Subscribe_ListString_Responses> responseObserver) {
        new Thread(() -> {
            final List<SiLAFramework.String> list = new ArrayList<SiLAFramework.String>() {{
                add(SiLAString.from("Element 0"));
                add(SiLAString.from("Element 1"));
                add(SiLAString.from("Element 2"));
            }};

            try {
                for (int i = 0; i < ObservableProperty.NB_RESPONSE; ++i) {
                    list.add(SiLAString.from("Element " + list.size()));
                    responseObserver.onNext(ObservablePropertyTestOuterClass.Subscribe_ListString_Responses
                            .newBuilder()
                            .addAllListString(list)
                            .build()
                    );
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (StatusRuntimeException ignored) {
                log.info("Subscription to observable property canceled.");
            }
        }).start();
    }

    @Override
    public void subscribeSingletonListString(ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Parameters request, StreamObserver<ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Responses> responseObserver) {
        try {
            responseObserver.onNext(ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Responses
                    .newBuilder()
                    .addSingletonListString(SiLAString.from("Best pattern"))
                    .build());
        } catch (StatusRuntimeException ignored) {
            log.info("Subscription to observable property canceled.");
        }
    }
}
