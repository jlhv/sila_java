package sila_java.examples.test_server.impl;

import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.parameterconstraintstest.v1.ParameterConstraintsTestGrpc;
import sila2.ch.unitelabs.test.parameterconstraintstest.v1.ParameterConstraintsTestOuterClass;

@Slf4j
public class ParameterConstraints extends ParameterConstraintsTestGrpc.ParameterConstraintsTestImplBase {
    @Override
    public void constrainedStringParameters(
            @NonNull final ParameterConstraintsTestOuterClass.ConstrainedStringParameters_Parameters request,
            @NonNull final StreamObserver<ParameterConstraintsTestOuterClass.ConstrainedStringParameters_Responses> responseObserver
    ) {
        responseObserver.onCompleted();
    }

    @Override
    public void constrainedNumericParameters(
        @NonNull final ParameterConstraintsTestOuterClass.ConstrainedNumericParameters_Parameters request,
        @NonNull final StreamObserver<ParameterConstraintsTestOuterClass.ConstrainedNumericParameters_Responses> responseObserver
    ) {
        responseObserver.onCompleted();
    }

    @Override
    public void constrainedDateTimeParameters(
        ParameterConstraintsTestOuterClass.ConstrainedDateTimeParameters_Parameters request,
        StreamObserver<ParameterConstraintsTestOuterClass.ConstrainedDateTimeParameters_Responses> responseObserver
    ) {
            responseObserver.onCompleted();
    }

    @Override
    public void constrainedListParameters(
        ParameterConstraintsTestOuterClass.ConstrainedListParameters_Parameters request,
        StreamObserver<ParameterConstraintsTestOuterClass.ConstrainedListParameters_Responses> responseObserver
    ) {
            responseObserver.onCompleted();
    }
}
