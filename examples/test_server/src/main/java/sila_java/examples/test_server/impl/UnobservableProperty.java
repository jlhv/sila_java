package sila_java.examples.test_server.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestGrpc;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
public class UnobservableProperty extends UnobservablePropertyTestGrpc.UnobservablePropertyTestImplBase {
    public static final int NB_ELEMENT_HUGE_LIST = 1000;
    public static final String[] STRING_ARRAY = {
            "Switzerland", "Germany", "France", "Italy", "Finland", "Russia", "Latvia", "Lithuania", "Poland",
            "Portugal", "Follow", "the White", "Rabbit"
    };
    public static final List<SiLAFramework.String> HUGE_LIST;

    static {
        HUGE_LIST = new ArrayList<>(NB_ELEMENT_HUGE_LIST);
        for (int i = 0; i < NB_ELEMENT_HUGE_LIST; i++) {
            final StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < i % 5; j++) {
                stringBuilder.append(STRING_ARRAY[(i >> j) % STRING_ARRAY.length]).append(" ");
            }
            HUGE_LIST.add(SiLAString.from(stringBuilder.toString()));
        }
    }

    public static final UnobservablePropertyTestOuterClass.Get_ListString_Responses LIST_STRING_RESPONSE =
            UnobservablePropertyTestOuterClass.Get_ListString_Responses.newBuilder()
                    .addListString(SiLAString.from("White"))
                    .addListString(SiLAString.from("Rabbit"))
            .build();

    public static final UnobservablePropertyTestOuterClass.Get_ListStructPairString_Responses LIST_STRUCT_PAIR_STRING_RESPONSE = UnobservablePropertyTestOuterClass.Get_ListStructPairString_Responses.newBuilder()
            .addListStructPairString(
                    UnobservablePropertyTestOuterClass.Get_ListStructPairString_Responses.ListStructPairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("Unite"))
                            .setValue(SiLAString.from("Labs"))
                            .build()
            ).addListStructPairString(
                    UnobservablePropertyTestOuterClass.Get_ListStructPairString_Responses.ListStructPairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("SiLA"))
                            .setValue(SiLAString.from("Standard"))
                            .build()
            ).build();

    public static final UnobservablePropertyTestOuterClass.Get_StructPairString_Responses STRUCT_PAIR_STRING_RESPONSE = UnobservablePropertyTestOuterClass.Get_StructPairString_Responses
            .newBuilder()
            .setStructPairString(UnobservablePropertyTestOuterClass.Get_StructPairString_Responses.StructPairString_Struct
                    .newBuilder()
                    .setPairString(UnobservablePropertyTestOuterClass.Get_StructPairString_Responses.StructPairString_Struct.PairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("AKey"))
                            .setValue(SiLAString.from("AValue"))
                            .build())
                    .build())
            .build();

    public static final UnobservablePropertyTestOuterClass.Get_DataType_Responses DATA_TYPE_RESPONSE = UnobservablePropertyTestOuterClass.Get_DataType_Responses
            .newBuilder()
            .setDataType(UnobservablePropertyTestOuterClass.DataType_DataTypeString
                    .newBuilder()
                    .setDataTypeString(SiLAString.from("blah"))
                    .build())
            .build();

    @Override
    public void getHugeList(UnobservablePropertyTestOuterClass.Get_HugeList_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_HugeList_Responses> responseObserver) {
        responseObserver.onNext(
                UnobservablePropertyTestOuterClass.Get_HugeList_Responses
                        .newBuilder()
                        .addAllHugeList(HUGE_LIST)
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getBoolean(sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass.Get_Boolean_Parameters request,
                           io.grpc.stub.StreamObserver<sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass.Get_Boolean_Responses> responseObserver) {
        responseObserver.onNext(UnobservablePropertyTestOuterClass.Get_Boolean_Responses.newBuilder().setBoolean(SiLABoolean.from(Math.random() < 0.5)).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getListString(UnobservablePropertyTestOuterClass.Get_ListString_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_ListString_Responses> responseObserver) {
        responseObserver.onNext(UnobservableProperty.LIST_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getRandomChangingListString(UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Responses> responseObserver) {
        final UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Responses.Builder newListRes =
                UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Responses.newBuilder();

        getNewList().forEach(item -> newListRes.addRandomChangingListString(SiLAString.from(item)));
        responseObserver.onNext(newListRes.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getListStructPairString(UnobservablePropertyTestOuterClass.Get_ListStructPairString_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_ListStructPairString_Responses> responseObserver) {
        responseObserver.onNext(UnobservableProperty.LIST_STRUCT_PAIR_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getStructPairString(UnobservablePropertyTestOuterClass.Get_StructPairString_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_StructPairString_Responses> responseObserver) {
        responseObserver.onNext(UnobservableProperty.STRUCT_PAIR_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getDataType(UnobservablePropertyTestOuterClass.Get_DataType_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_DataType_Responses> responseObserver) {
        responseObserver.onNext(DATA_TYPE_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getThrowException(UnobservablePropertyTestOuterClass.Get_ThrowException_Parameters request, StreamObserver<UnobservablePropertyTestOuterClass.Get_ThrowException_Responses> responseObserver) {
        responseObserver.onError(
                SiLAErrors.generateDefinedExecutionError(
                        "TestException",
                        "Nothing to do, this will always throw an exception"
                )
        );
    }

    /**
     * getNewList()
     * Randomly generates a list of strings, based on our STRING_ARRAY
     * @return List of Strings
     */
    private static List<String> getNewList() {
        final List<String> listString = new ArrayList<>();
        final Random random = new Random();
        final int listLength = random.nextInt(STRING_ARRAY.length);
        for (int i = 0; i < listLength; i++) {
            listString.add(STRING_ARRAY[random.nextInt(STRING_ARRAY.length)]);
        }
        return listString;
    }
}
