package sila_java.examples.test_server;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import junit.framework.Assert;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.jupiter.api.Test;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.types.SiLAString;

import java.io.IOException;

@Slf4j
class TestAnyType extends TestBase {
    private static final JsonParser JSON_PARSER = new JsonParser();

    @Test
    void testAnonymousList() {
        val expected = JSON_PARSER.parse("{\"value\":[{\"type\":\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"yes\\\"?>\\n<dataTypeType>\\n    <Basic>Integer</Basic>\\n</dataTypeType>\\n\",\"payload\":\"CIkG\"},{\"type\":\"<DataType><Basic>String</Basic></DataType>\",\"payload\":\"CgRTaUxB\"}]}");
        val actual = getJsonOfType("AnonymousList");
        Assert.assertEquals(expected, actual);
    }

    @Test
    void testAnonymousStructure() {
        val expected = JSON_PARSER.parse("{X: {value: \"101\"}}");
        val actual = getJsonOfType("AnonymousStructure");

        Assert.assertEquals(expected, actual);
    }

    @Test
    void testConstrainedReal() {
        val expected = JSON_PARSER.parse("{value: 7.7}");
        val actual = getJsonOfType("ConstrainedReal");

        Assert.assertEquals(expected, actual);
    }

    @Test
    void testInteger() {
        val expected = JSON_PARSER.parse("{value: \"1337\"}");
        val actual = getJsonOfType("Integer");

        Assert.assertEquals(expected, actual);
    }

    @Test
    void testString() {
        val expected = JSON_PARSER.parse("{value: \"test\"}");
        val actual = getJsonOfType("String");

        Assert.assertEquals(expected, actual);
    }

    @SneakyThrows({MalformedSiLAFeature.class, IOException.class})
    private JsonElement getJsonOfType(@NonNull final String type) {
        val response = unobservableCommandStub.valueForTypeProvider(
                UnobservableCommandTestOuterClass.ValueForTypeProvider_Parameters
                        .newBuilder()
                        .setType(SiLAString.from(type))
                        .build()
        );

        return JSON_PARSER.parse(ProtoMapper.serializeToJson(SiLAAny.toMessage(response.getAny())));
    }
}
